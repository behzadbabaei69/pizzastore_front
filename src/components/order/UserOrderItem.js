import React, {useContext} from 'react'
import {CurrencyContext} from "../../contexts/CurrencyContext";

const UserOrderItem = ({order}) => {
    const {defaultCurrency} = useContext(CurrencyContext)
    return (
        <tr style={{textAlign:'center'}}>
            <td>{order.id} </td>
            <td>{defaultCurrency.symbol} {order.total_price} </td>
            <td>{defaultCurrency.symbol} {order.delivery_cost} </td>
            <td>{order.order_items.length} </td>
            <td>{defaultCurrency.symbol} {order.total_cost} </td>
            <td>{(order.is_delivery) ? 'Yes' : 'No'} </td>
            <td>{defaultCurrency.symbol} {order.total_discount}</td>
            <td>{order.tracking_number} </td>
        </tr>
    );
}

export default UserOrderItem;