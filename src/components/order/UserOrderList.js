import React, {useContext} from 'react'
import {UserContext} from "../../contexts/UserContext";
import UserOrderItem from "./UserOrderItem";

const UserOrderList = () => {
    const {userOrders} = useContext(UserContext)
    return (

            <table className="table thead-light">
                <thead>
                <tr style={{textAlign:'center'}}>
                    <th>Order ID</th>
                    <th>Cost</th>
                    <th>Delivery Cost</th>
                    <th>Items</th>
                    <th>Total Cost</th>
                    <th>Is Delivery</th>
                    <th>Discount</th>
                    <th>Tracking Number</th>
                </tr>
                </thead>
                <tbody>
                { userOrders.length?(
                    userOrders.map((order)=>{
                        return (
                            <UserOrderItem order={order} />
                        )
                })
                    ):(
                    <tr style={{textAlign:'center'}} >
                        <td colSpan="8">There is no previous order!</td>
                    </tr>
                   )

                }
                </tbody>
            </table>
    );
}

export default UserOrderList;