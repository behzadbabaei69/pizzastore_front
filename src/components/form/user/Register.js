import React, {useContext, useState} from 'react'
import {useToasts} from "react-toast-notifications";
import {UserContextDispatch} from "../../../contexts/UserContext";

const RegisterForm = ({cancelDialog}) => {

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const {addToast} = useToasts()
    const {registerUser} = useContext(UserContextDispatch)
    const handleFirstNameChange = (firstName) => {
        setFirstName(firstName)
    }

    const handleLastNameChange = (lastName) => {
        setLastName(lastName)
    }

    const handlePhoneNumberChange = (phoneNumber) => {
        if (phoneNumber.match(/^[0-9]+$/)) {
            setPhoneNumber(phoneNumber)
        }
    }

    const handleEmailChange = (email) => {
        setEmail(email)
    }

    const handlePasswordChange = (password) => {
        setPassword(password)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        if (email.length < 6) {
            addToast("Email is required and not less than 6 character", {
                appearance: 'error',
                autoDismiss: true,
            })
            return 0;
        }
        if (password.length < 6) {
            addToast("password is required and not less than 6 character", {
                appearance: 'error',
                autoDismiss: true,
            })

            return 0;
            //toast error
        }

        let userData = {
            first_name: firstName,
            last_name: lastName,
            phone_number: phoneNumber,
            email: email,
            password: password,
        }
        // registerUser(userData)

        let flag = registerUser(userData);

        if (flag) {
            cancelDialog()
        }
    }

    return (
        <>
            <form className="form register-form">
                <div className="form-group">
                    <label className="col-md-4 col-form-label" htmlFor="firstName">First Name</label>
                    <input className="col-md-7 form-control" placeholder="first name ...." type="text" name="firstName" value={firstName}
                           onChange={(e) => handleFirstNameChange(e.target.value)}/>
                </div>

                <div className="form-group">
                    <label className="col-md-4 col-form-label" htmlFor="lastName">Last Name</label>
                    <input className="col-md-7 form-control" type="text"
                           placeholder="last name ...."
                           name="lastName" value={lastName}
                           onChange={(e) => handleLastNameChange(e.target.value)}/>
                </div>

                <div className="form-group">
                    <label className="col-md-4 col-form-label" htmlFor="phoneNumber">Phone Number</label>
                    <input className="col-md-7 form-control" type="text"
                           placeholder="only numbers 0-9"
                           name="phoneNumber" value={phoneNumber}
                           onChange={(e) => handlePhoneNumberChange(e.target.value)}/>
                </div>

                <div className="form-group">
                    <label className="col-md-4 col-form-label" htmlFor="email">Email</label>
                    <input className="col-md-7 form-control" type="text"
                           placeholder="email@example.com"
                           name="email" aria-required="true" value={email}
                           onChange={(e) => handleEmailChange(e.target.value)}/>
                </div>

                <div className="form-group">
                    <label className="col-md-4 col-form-label" htmlFor="password">Password</label>
                    <input className="col-md-7 form-control"
                           placeholder="password...."
                           type="text" name="password" aria-required="true"
                           value={password}
                           onChange={(e) => handlePasswordChange(e.target.value)}/>
                </div>

                <div className="form-group form-buttons">
                    <button className="btn add-button" onClick={(e) => handleSubmit(e)} value="Register">Register</button>
                    {(cancelDialog) ?
                        (
                            <button className="btn btn-secondary" onClick={(e) => {
                                e.preventDefault();
                                cancelDialog()
                            } }>Cancel</button>
                        ) : (<></>)
                    }
                </div>


            </form>


        </>
    );
}

export default RegisterForm;