import React, {useContext} from 'react';
import {CartContext, CartContextDispatch} from "../../contexts/CartContext";
import {CurrencyContext} from "../../contexts/CurrencyContext";
import {CheckoutContext, CheckoutContextDispatch} from "../../contexts/CheckoutContext";


export default function CheckoutInfo() {

    const {
        cart,
        payments,
        isDelivery,
        defaultPayment
    } = useContext(CartContext);

    const {
        handleIsDeliveryChange,
        handleDefaultPaymentChange
    } = useContext(CartContextDispatch)

    const {
        phoneNumber,
        deliveryAddress,
        contactName
    } = useContext(CheckoutContext)

    const {
        handlePhoneNumberChange,
        handleContactNameChange,
        handleDeliveryAddressChange
    } = useContext(CheckoutContextDispatch)

    const {defaultCurrency} = useContext(CurrencyContext);


    return (
        <>
            <table className="table table-borderless checkout-table-info">
                <tbody>
                <tr>
                    <td>Order Summary</td>
                    <td>Pizza Orders</td>
                </tr>
                <tr>
                    <td>Items</td>
                    <td>{cart.totalItem}</td>
                </tr>
                <tr>
                    <td>Fee</td>
                    <td>{defaultCurrency.symbol} {cart.totalFee}</td>
                </tr>

                <tr>
                    <td>Delivery Cost</td>
                    <td>{defaultCurrency.symbol} {cart.deliveryCost}</td>
                </tr>


                <tr>
                    <td><b>Total</b></td>
                    <td><b>{defaultCurrency.symbol} {cart.totalCost}</b></td>
                </tr>

                </tbody>
            </table>

            <table className="table table-borderless checkout-table-info">
                <tbody>
                <tr>
                    <td>Pickup Type</td>
                    <td>
                        <div className="form-group">

                            <input id="radDeliver" onChange={() => handleIsDeliveryChange(true)}
                                   checked={(isDelivery)} name="delivery_type" type="radio"/>
                            <label htmlFor="radDeliver">Deliver</label>
                            <span>{'   '}</span>

                            <input id="radPickup" onChange={() => handleIsDeliveryChange(false)}
                                   checked={(!isDelivery)} name="delivery_type" type="radio"/>
                            <label htmlFor="radPickup">Pickup</label>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>

            <div className="row">
                <div className="col-md-12">
                    <div className="form-group">
                        <label htmlFor="phoneNumber">Phone Number:</label>
                        <input id="phoneNumber" className="form-control" type="text" value={phoneNumber}
                               onChange={(e) => handlePhoneNumberChange(e.target.value)}/>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-md-12">
                    <div className="form-group">
                        <label htmlFor="contactName">Contact Name:</label>
                        <input id="contactName" className="form-control" type="text" value={contactName}
                               onChange={(e) => handleContactNameChange(e.target.value)}/>
                    </div>
                </div>
            </div>

            {isDelivery ? (
                <>
                    <div className="row">
                        <div className="col-md-12">Delivery Address:</div>
                    </div>
                    <div className="row">

                        <div className="col-md-12">
                            <div className="form-group">
                                <textarea className="form-control" style={{width: '100%'}}
                                          placeholder="address..."
                                          onChange={(e) => handleDeliveryAddressChange(e.target.value)}
                                          value={deliveryAddress}/>
                            </div>
                        </div>
                    </div>
                </>
            ) : (
                <>
                </>
            )
            }


            <div className="row" style={{marginTop: '15px'}}>
                <div className="col-md-12">Payment</div>
            </div>

            <div className="row" style={{marginBottom: '10px'}}>

                {payments.map((cPayment) => {
                    return (
                        <div key={cPayment.id}
                             className={"col-md-4 payment-list-item " + (cPayment.id === defaultPayment.id ? "active-payment" : "")}>
                            <a onClick={() => (handleDefaultPaymentChange(cPayment))}>
                                <img src={cPayment.logo} alt={cPayment.name}/>
                            </a>
                        </div>
                    );
                })}
            </div>

        </>
    )
        ;
}