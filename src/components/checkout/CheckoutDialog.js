import React, {useContext, useState} from 'react';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import CheckoutInfo from "./CheckoutInfo";
import {CheckoutContextDispatch} from "../../contexts/CheckoutContext";

export default function CheckoutDialog(props) {
    const {
        className
    } = props;

    const {sendOrdersToServer} = useContext(CheckoutContextDispatch)
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);
    const orderStoreHandler = () => {
       let response = sendOrdersToServer();
       if(response) toggle()
    }

    return (
        <div>
            <a className="btn add-button" onClick={toggle}>Check Out</a>
            <Modal isOpen={modal} toggle={toggle} className={className}>
                <ModalHeader toggle={toggle}>Check Out</ModalHeader>
                <ModalBody>
                    <CheckoutInfo/>
                </ModalBody>
                <ModalFooter>
                    <a className="btn add-button" onClick={orderStoreHandler}>Pay</a>
                    <Button onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}