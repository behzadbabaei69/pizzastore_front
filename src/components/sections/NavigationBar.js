import React, {useContext, useEffect, useState} from 'react'
import {CurrencyContext} from "../../contexts/CurrencyContext";
import logo from '../../stylesheet/images/logo.png';

import {
    Collapse,
    Navbar,
    NavLink,
    NavbarToggler,
    NavItem,
    Nav,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
} from 'reactstrap';
import RegisterDialog from "../user/RegisterDialog";
import {UserContext} from "../../contexts/UserContext";
import {useToasts} from "react-toast-notifications";


const NavigationBar = () => {
    const {defaultCurrency, currencies, changeCurrency} = useContext(CurrencyContext);

    const [menuIsOpen, setMenuIsOpen] = useState(false);
    const [registerModal, setRegisterModal] = useState(false);
    const {user} = useContext(UserContext)
    const {addToast} = useToasts()
    const menuToggle = () => setMenuIsOpen(!menuIsOpen);
    const registerDialogToggle = () => {
        if((user !== undefined) && (user !== null)) {
            setRegisterModal(false);
            addToast("You are already registered!", {
                appearance: 'error',
                autoDismiss: true,
            })
        } else {
            setRegisterModal(!registerModal);
        }
    }

    useEffect(() => {
        if ((user !== undefined) && (user !== null)) {
            setRegisterModal(false);
        }
    }, [user])


    return (
        <>
            <RegisterDialog toggle={registerDialogToggle} modal={registerModal} setModal={setRegisterModal}/>

            <Navbar color="light" direction="right" light expand="md">

                <NavbarToggler onClick={menuToggle}/>
                <Collapse isOpen={menuIsOpen} navbar>

                    <Nav className="navbar-left" navbar>
                        <NavItem>
                            <NavLink href={'/'} to={'/'} className="nav-link">
                                <img className="pizza-logo" src={logo} alt="LOGO"/>
                            </NavLink>
                        </NavItem>

                        <NavItem>
                            <NavLink href={'/'} to={'/'} className="nav-link">
                                Home
                            </NavLink>
                        </NavItem>

                        <NavItem>
                            <NavLink href={'/about'} to={'/about'} className="nav-link">
                                About US
                            </NavLink>
                        </NavItem>


                    </Nav>

                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink onClick={registerDialogToggle} className="nav-link">
                                Sign Up
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href={'/user/orders'} to={'/user/orders'} className="nav-link">
                                My Orders
                            </NavLink>
                        </NavItem>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav caret>
                                {defaultCurrency.symbol} {defaultCurrency.code}
                            </DropdownToggle>
                            <DropdownMenu right>

                                {currencies.map((currency) => {
                                    if ((currency.code !== defaultCurrency.code)) {
                                        return (
                                            <DropdownItem key={currency.code}
                                                          onClick={() => changeCurrency(currency.code)}>
                                                {currency.symbol} {currency.code}
                                            </DropdownItem>
                                        );
                                    }
                                })}

                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                </Collapse>
            </Navbar>
        </>
    );
}

export default NavigationBar;