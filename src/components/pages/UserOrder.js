import React from 'react'
import UserOrderList from "../order/UserOrderList";

const UserOrder = () => {

    return (
        <section className="orders-section">
            <div className="container-fluid">
                <h1 style={{textAlign: 'center'}}>My Orders</h1>
                <UserOrderList />
            </div>
        </section>

    );
}

export default UserOrder;