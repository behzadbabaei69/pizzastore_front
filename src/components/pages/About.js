import React from 'react'

const About = () => {
    return (
        <section className="about-section" style={{marginBottom:'60px'}}>
            <div className="container-fluid">
                <div className="row">
                    <div className="sqs-block-content">
                        <h2 style={{whiteSpace:'pre-wrap',textAlign:'center'}}>
                            <strong>-Serving First Class Pizza for Over 30 Years!-</strong>
                        </h2>
                        <p className=""  style={{whiteSpace:'pre-wrap'}}></p>
                        <h1 style={{whiteSpace:'pre-wrap',textAlign:'center'}}>About Us</h1>

                        <p className="" style={{whiteSpace:'pre-wrap'}}>
                        <strong>The Pizza Store More</strong>
                            is a dine-in family owned pizza restaurant located
                        in the heart of Munchen - Germany - The Pizza Store. The local residents love coming to the restaurant to enjoy some of our
                        popular menu items such as our BBQ chicken pizza, buffalo chicken wings, mozzarella sticks,
                        anitpasto salad and more. </p>

                        <p className="" style={{whiteSpace:'pre-wrap'}}>The Pizza
                        Store More has established a reputation for providing the highest quality food, excellent
                        customer service and fast delivery to customers in the Munchen area . We make sure our prices
                        are affordable for all individuals and families . We can promise full satisfaction since all of
                        our dishes at The Pizza Store are freshly prepared every day with high quality ingredients. </p>
                        <h1 style={{whiteSpace:'pre-wrap' ,textAlign:'center'}}>History</h1>
                        <p className="" style={{whiteSpace:'pre-wrap'}}>The
                            Pizza Store More has been serving pizza lovers for over 30 years. Established at
                            Munchen in 1990 .Our customers come from Munchen, Berlin, Frankfurt and all over Orange County. </p>
                        <h1 style={{whiteSpace:'pre-wrap' ,textAlign:'center'}}>We Invite You…</h1>
                        <p className="" style={{whiteSpace:'pre-wrap'}}>The pizza Store More
                            invites you to come join us at our first class pizza restaurant. We are conveniently located
                            in Munchen - Germany. Speaking for the entire staff at The
                            Pizza Store, we appreciate our customers for their loyalty and past patronage. Going
                            forward, we promise to continue to maintain a high quality operation to serve you in the
                            future.</p>
                    </div>
                </div>
            </div>
        </section>

    );
}

export default About;