import React from 'react'
import PizzaContextProvider from "../../contexts/PizzaContext";
import Pizza from "../pizza/Pizza";

const Home = () => {
    return (
        <section className="home-section">
            <div className="container-fluid">
                <PizzaContextProvider>
                    <Pizza/>
                </PizzaContextProvider>
            </div>

        </section>

    );
}

export default Home;