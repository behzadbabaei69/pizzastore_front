import React from 'react';
import {Modal, ModalHeader, ModalBody} from 'reactstrap';
import RegisterForm from "../form/user/Register";

export default function RegisterDialog(props) {
    const {
        className,
        toggle,
        modal,
        setModal
    } = props;

    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-12">
                    <Modal isOpen={modal} toggle={toggle} className={className}>
                        <ModalHeader toggle={toggle}>Register</ModalHeader>
                        <ModalBody>
                            <RegisterForm cancelDialog={toggle} />
                        </ModalBody>
                    </Modal>
                </div>
            </div>

        </div>
    );
}