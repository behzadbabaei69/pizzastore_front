import React, {useContext} from "react";
import uuid from "uuid/dist/v1";
import {CurrencyContext} from "../../contexts/CurrencyContext";
import {OrderContextDispatch} from "../../contexts/OrderContext";


const PizzaDetail = ({pizza}) => {
    const {addOrder} = useContext(OrderContextDispatch);
    const {defaultCurrency} = useContext(CurrencyContext);

    const handleItemAdd = (pizzaItem) => {
        let item = Object.assign({},pizzaItem)
        let totalPrice = 0.0;
        let pizzaType = 0.0;
        let quantity = 1;
        item.pizzaTypes.map((pizza) => {
            if (pizza.isDefault) {
                totalPrice = quantity * pizza.price;
                pizzaType = pizza.name;
                // item.defaultPizzaType =  Object.assign({},pizza);
            }
        });

        let orderObj = {
            id: uuid(),
            quantity,
            pizzaType,
            totalPrice,
            order: item,
        };

        addOrder(orderObj);
    };

    return (

        <div className="row pizza-item-row">
            <div className="col-4 text-center pizza-item-left">
                <div className="row align-items-center">
                    <div className="d-flex justify-content-center justify-content-md-start ghost-hidden w-100">
                        <span className="align-self-center">  </span>
                    </div>
                    <div className="w-100 pizza-image-wrapper">
                        <img className="img pizza-item-image ghost-hidden pp-image-loaded" alt={pizza.name}
                             src={pizza.thumbImgUrl}/>
                    </div>
                    <div className="d-flex justify-content-center justify-content-md-start ghost-hidden w-100"></div>
                </div>
            </div>
            <div className="col-8 individual-product-right pt-4 pb-3 ">
                <span className="active-checkmark"> </span>
                <div className="row h-100">
                    <div className="col-12 align-self-center pizza-item-header">
                        <h4 className="fw-black"> {pizza.name} </h4></div>
                    <div className="col-12 align-self-end">
                        <div className="row">
                            <div className="col-12">
                                <p className="ghost-bar pizza-description">
                                    {pizza.description}
                                </p>
                            </div>
                        </div>
                        <div className="row no-margin align-items-end justify-content-between">
                            <div className="col-12 text-left col-md-7">
                                <div className="h6 fw-extra-bold">
                                    <span> Starting from {defaultCurrency.symbol} {pizza.defaultPizzaType.price}</span>
                                </div>
                            </div>
                            <div className="col-12 text-right col-md-5">
                                <div className="row justify-content-md-end ">
                                    <a className="btn add-button" onClick={() => handleItemAdd(pizza)}>
                                        Add
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PizzaDetail;
