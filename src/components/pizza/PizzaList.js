import React, {useContext} from 'react'
import {PizzaContext} from "../../contexts/PizzaContext";
import PizzaDetail from "./PizzaDetail";


const PizzaList = () => {

    const {pizzas} = useContext(PizzaContext)

    return (
        <>
            <div className="pizza-list">
                {pizzas.map((pizza) => {
                    return (
                        <PizzaDetail key={pizza.id} pizza={pizza}/>
                    );
                })}
            </div>
        </>

    );
}

export default PizzaList;