import React from 'react'
import Cart from "../cart/Cart";
import PizzaList from "./PizzaList";

const Pizza = () => {
    return (
          <div className="row">
              <div className="col-sm-6 col-md-4">
                  <Cart />
              </div>
              <div className="col-sm-6 col-md-8">
                  <PizzaList />
              </div>
          </div>
    );
}

export default Pizza;