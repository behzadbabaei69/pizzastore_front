import React, {useContext} from "react";
import {CartContext} from "../../contexts/CartContext";
import CheckoutDialog from "../checkout/CheckoutDialog";
import {CurrencyContext} from "../../contexts/CurrencyContext";

const CartSummery = () => {
    const {cart} = useContext(CartContext);
    const {defaultCurrency} = useContext(CurrencyContext);

    return (
        <>
            <div className="cart-summery">
                <div className="payment-details">
                    <div className="total-price">Order Summary</div>
                    <table className="table table-borderless">
                        <tbody>
                        <tr>
                            <td>Order Summary</td>
                            <td>Pizza Orders</td>
                        </tr>

                        <tr>
                            <td>Items</td>
                            <td>{cart.totalItem} </td>
                        </tr>
                        <tr>
                            <td>Fee</td>
                            <td>{defaultCurrency.symbol} {cart.totalFee}</td>
                        </tr>
                        <tr>
                            <td>Cost</td>
                            <td>{defaultCurrency.symbol} {cart.cost}</td>
                        </tr>
                        <tr>
                            <td>Delivery Cost</td>
                            <td>{defaultCurrency.symbol} {cart.deliveryCost}</td>
                        </tr>
                        <tr>
                            <td><b>Total</b></td>
                            <td>
                                <b>{defaultCurrency.symbol} {cart.totalCost}</b>
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="2">
                                <CheckoutDialog/>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    )
};

export default CartSummery;
