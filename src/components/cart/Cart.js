import React, { useContext } from "react";
import CartItem from "./CartItem";
import {OrderContext} from "../../contexts/OrderContext";

const Cart = () => {
  const { orders } = useContext(OrderContext);

  return !orders.length ? (
    <div className="cart">
      <div className="preorder-container">
        <div className="preorder-header">
          <span className="timeIcon"></span>
          Pre Order
        </div>
      </div>

      <div id="empty-basket">Empty Cart</div>
    </div>
  ) : (
    <CartItem />
  );
};

export default Cart;
