import React, {useContext} from "react";
import {CurrencyContext} from "../../contexts/CurrencyContext";
import CartSummery from "./CartSummery";
import {OrderContext, OrderContextDispatch} from "../../contexts/OrderContext";

const CartItem = () => {
    const {orders} = useContext(OrderContext);
    const {
        changeOrderType,
        addOneOrderQuantity,
        minOneOrderQuantity,
        deleteOrder,
    } = useContext(OrderContextDispatch);

    const {defaultCurrency} = useContext(CurrencyContext);

    return (
        <div className="cart">

            <CartSummery />
            
            <div className="preorder-container">
                <div className="preorder-header">
                    <span className="timeIcon"></span>
                    Pre Order
                </div>
            </div>

            {orders.map((item) => {
                return (
                    <div key={item.id} className="cart-item">
                        <div className="basket-header">
                          <span className="basket-header-span">
                            <span>{item.quantity}</span>
                          </span>
                            Cart
                        </div>

                        <div className="basket-items container-fluid">
                            <div className="basket-item row idn-basket-hover"
                                 style={{display: "block"}}>
                                <div className="basket-item-title col-xs-12">
                                    {item.order.name}
                                </div>

                                <div className="row product-size-picker-row col-xs-12">
                                    {item.order.pizzaTypes.map((pizzaType) => {
                                        return (
                                            <button
                                                onClick={() => changeOrderType(item, pizzaType)}
                                                key={pizzaType.id}
                                                className={
                                                    "col-3 text-center size-instance " +
                                                    (pizzaType.id === item.order.defaultPizzaType.id
                                                        ? "active"
                                                        : "")
                                                }>
                                                <div className="label"> {pizzaType.sizeName}</div>
                                                <div className="separator"></div>
                                                <div className="label mt-2">
                                                    {pizzaType.slice} Slices
                                                </div>
                                            </button>
                                        );
                                    })}
                                </div>

                                <div className="basket-item-action row">
                                    <div className="basket-item-price col-md-5 col-xs-5">

                                        <span>Price</span>
                                        <span className="price-holder">
                                            {defaultCurrency.symbol}
                                            {item.totalPrice ? item.totalPrice : 0}
                                        </span>
                                    </div>
                                    <div className="basket-item-price col-md-5 col-xs-5">
                                        <a disabled={item.quantity === 1 ? "disabled" : ""}
                                           className="inc action-btn"
                                           onClick={() => minOneOrderQuantity(item)}>
                                            -
                                        </a>

                                        <span className="quantity" qnt="1">
                                          <span className="kk-fa-digit kk-fa-digit-done">
                                            {item.quantity}
                                          </span>
                                        </span>

                                        <a className="dec action-btn"
                                           onClick={() => addOneOrderQuantity(item)}>
                                            +
                                        </a>



                                    </div>

                                    <div className="basket-item-price col-md-2 col-xs-2">
                                        <a   onClick={() => deleteOrder(item)}>
                                            <span className="removeIcon"></span>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                );
            })}
        </div>
    );
};

export default CartItem;
