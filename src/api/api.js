import axios from 'axios';
// import cookie from 'js-cookie';
import config from '../config.json';

// const token = 'Bearer ' + cookie.get('accessToken')

//production url   "base_url": "http://pizzadmin.worldnotifier.com",
//local  url   "base_url": "http://www.pizzastore.loc",

const axiosBaseUrl = config.base_url + '' + config.api_prefix

export const api = axios.create({
    baseURL: axiosBaseUrl
})
