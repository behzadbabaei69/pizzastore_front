import React, {createContext, useContext, useEffect, useState} from "react";
import paypalLogo from "../stylesheet/images/payments/paypal.png";
import sofortLogo from "../stylesheet/images/payments/sofort.png";
import paysafecardLogo from "../stylesheet/images/payments/paysafecard.png";
import {OrderContext} from "./OrderContext";

export const CartContext = createContext();
export const CartContextDispatch = createContext();


const CartContextProvider = (props) => {

    const {orders} = useContext(OrderContext)
    const [isDelivery, setIsDelivery] = useState(false);

    let initialCartObject = {
        totalItem: 0,
        cost: 0.0,
        totalCost: 0.0,
        totalFee: 0.0,
        deliveryCost: 0.0
    }
    const [payments, setPayments] = useState([
        {
            id: 1,
            name: 'Paypal',
            logo: paypalLogo,
        },
        {
            id: 2,
            name: 'sofort',
            logo: sofortLogo,
        },
        {
            id: 3,
            name: 'Paysafecard',
            logo: paysafecardLogo,
        }
    ]);

    const [defaultPayment, setDefaultPayment] = useState(payments[0])
    const [cart, setCart] = useState(initialCartObject)

    const handleIsDeliveryChange = (status) => {
        setIsDelivery(status);
    }

    const resetCart = () => {
        setCart(initialCartObject);
    }

    const handleDefaultPaymentChange = (payment) => {
        setDefaultPayment(payment)
    }

    const updateCart = () => {
        let cartObj = {}
        cartObj.totalCost = 0
        cartObj.cost = 0
        cartObj.totalItem = 0
        cartObj.deliveryCost = 0.0
        orders.map((item) => {
            cartObj.cost = (cartObj.cost + parseFloat(item.totalPrice));
            cartObj.totalItem = cartObj.totalItem + item.quantity
        });
        cartObj.totalFee = 0;
        cartObj.cost = cartObj.cost.toFixed(2)
        if (isDelivery) {
            cartObj.deliveryCost = ((cartObj.cost / 100) * 10);
            cartObj.deliveryCost = cartObj.deliveryCost.toFixed(2);
        }
        cartObj.totalCost = parseFloat(cartObj.cost) + parseFloat(cartObj.deliveryCost)
        // console.log("deliveryCost", cartObj.deliveryCost,'totalCost',cartObj.totalCost);
        cartObj.totalCost = cartObj.totalCost.toFixed(2);
        setCart(cartObj);
    }

    useEffect(() => {
        updateCart()
    }, [isDelivery])

    useEffect(() => {
        updateCart()
    }, [orders])

    return (
        <CartContext.Provider value={{
            cart,
            payments,
            defaultPayment,
            isDelivery
        }}>
            <CartContextDispatch.Provider
                value={{
                    updateCart,
                    handleIsDeliveryChange,
                    handleDefaultPaymentChange,
                    resetCart
                }}>
                {props.children}
            </CartContextDispatch.Provider>
        </CartContext.Provider>
    );
};

export default CartContextProvider;
