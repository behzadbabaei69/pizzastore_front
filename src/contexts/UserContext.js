import React, {createContext, useContext, useEffect, useState} from "react";
import {api} from '../api/api'
import {useToasts} from "react-toast-notifications";
import useSWR from "swr";
import {ConfigContext} from "./ConfigContext";
import axios from 'axios'
import {CurrencyContext} from "./CurrencyContext";

export const UserContext = createContext();
export const UserContextDispatch = createContext();


const UserContextProvider = (props) => {

    const {addToast} = useToasts()
    const {userToken, setUserToken} = useContext(ConfigContext)
    const {defaultCurrency} = useContext(CurrencyContext)

    const [user, setUser] = useState(() => {
        const localData = localStorage.getItem('user')
        return (localData) ? JSON.parse(localData) : null
    })
    const fetcher = async (url) => await axios(url, {
        headers: {
            'ApiToken': userToken
        }
    }).then(r => r.data)
    // const fetcher = url => axios(url, ).then(r => r.json())

    let {data: userOrdersData, error} = useSWR('user/orders?currency=' + defaultCurrency.code, fetcher)

    const [userOrders, setUserOrders] = useState(() => {
        const localData = localStorage.getItem('userOrders')
        return (localData) ? JSON.parse(localData) : []
    })


    const registerUser = (userData) => {
        //check if user does not exist then register
        // otherwise throw a error toast

        let flag = false;

        userData.api_token = userToken;
        api.post('auth/register', userData, {
            headers: {
                'ApiToken': userToken
            }
        }).then(response => {
                if (response.data.status === 'success') {
                    setUser(response.data.data.user)
                    addToast(response.data.message, {
                        appearance: 'success',
                        autoDismiss: true,
                    })
                    flag = true;
                } else {
                    addToast(response.data.message, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                }
            })
        return flag;
    }

    useEffect(() => {
        if (userOrdersData !== undefined) {
            if (userOrdersData.status !== 'error') {
                setUserOrders(userOrdersData.data.orders);
            }
        }
    }, [userOrdersData])

    useEffect(() => {
        if (userOrders !== undefined) {
            localStorage.removeItem('userOrders')
            localStorage.setItem('userOrders', JSON.stringify(userOrders));
        }
    }, [userOrders])


    useEffect(() => {
        if ((user !== undefined) && (user !== null)) {
            setUserToken(user.api_token)
            localStorage.removeItem('user')
            localStorage.setItem('user', JSON.stringify(user));
        }
    }, [user])

    return (
        <UserContext.Provider value={{
            user,
            userOrders,
            userToken
        }}>
            <UserContextDispatch.Provider
                value={{
                    registerUser
                }}>
                {props.children}
            </UserContextDispatch.Provider>
        </UserContext.Provider>
    );
};

export default UserContextProvider;
