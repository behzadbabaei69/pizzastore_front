import React, {createContext, useContext, useState} from "react";
import {api} from '../api/api'
import {CurrencyContext} from "./CurrencyContext";
import {CartContext, CartContextDispatch} from "./CartContext";
import {useToasts} from "react-toast-notifications";
import {OrderContext, OrderContextDispatch} from "./OrderContext";
import {UserContext} from "./UserContext";


export const CheckoutContext = createContext();
export const CheckoutContextDispatch = createContext();


const CheckoutContextProvider = (props) => {

    const {isDelivery, cart, defaultPayment} = useContext(CartContext)
    const {resetCart} = useContext(CartContextDispatch)
    const {defaultCurrency} = useContext(CurrencyContext)
    const {addToast} = useToasts()
    const {orders} = useContext(OrderContext)
    const {setOrders} = useContext(OrderContextDispatch)
    const {userToken} = useContext(UserContext)

    const [deliveryAddress, setDeliveryAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [contactName, setContactName] = useState('');


    const restCheckoutForm = () => {
        setDeliveryAddress('');
        setPhoneNumber('');
        setContactName('')
    }

    const sendOrdersToServer = () => {

        console.log("phoneNumber:", phoneNumber);
        console.log("contactName:", contactName);
        console.log("deliveryAddress:", deliveryAddress);
        if ((phoneNumber === '') || (contactName === '') || ((isDelivery && (deliveryAddress === '')))) {
            addToast("Phone number,Delivery address and Contact name are required!", {
                appearance: 'error',
                autoDismiss: true,
            })

            return 0;
        }


        cart.is_delivery = isDelivery
        cart.payment = defaultPayment.id
        cart.deliver_address = deliveryAddress
        cart.phone_number = phoneNumber
        cart.contact_name = contactName
        let flag = false

        api.post('order', {
            orders: orders,
            cart: cart,
            currency: defaultCurrency.code
        }, {
            headers: {
                'ApiToken': userToken
            }
        }).then(response => {

            if (response.data.status === 'success') {
                flag = true;
                addToast(response.data.message, {
                    appearance: 'success',
                    autoDismiss: true,
                })
                setOrders([]);
                resetCart();
                restCheckoutForm()
                //then the order is saved successfully with tracking number
            } else {
                addToast(response.data.message, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }

        })
        return flag;
    }


    const handleContactNameChange = (name) => {
        setContactName(name);
    }

    const handlePhoneNumberChange = (phoneNumber) => {
        //there might be validation
        if (phoneNumber.match(/^[0-9]+$/)) {
            setPhoneNumber(phoneNumber)
        }

    }

    const handleDeliveryAddressChange = (address) => {
        setDeliveryAddress(address);
    }


    return (
        <CheckoutContext.Provider value={{
            deliveryAddress,
            phoneNumber,
            contactName

        }}>
            <CheckoutContextDispatch.Provider
                value={{
                    handlePhoneNumberChange,
                    handleDeliveryAddressChange,
                    handleContactNameChange,
                    sendOrdersToServer
                }}>
                {props.children}
            </CheckoutContextDispatch.Provider>
        </CheckoutContext.Provider>
    );
};

export default CheckoutContextProvider;
