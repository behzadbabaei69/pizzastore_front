import React, {createContext, useEffect, useState} from 'react'
import useSWR from 'swr'
import configJson from '../config.json';

export const CurrencyContext = createContext()


const CurrencyContextProvider = (props) => {

    let {data: currenciesData, error} = useSWR('currency');

    const [currencies, setCurrencies] = useState(() => {
        const localData = localStorage.getItem('currencies');
        return (localData) ? JSON.parse(localData) : configJson.currencies;
    })

    useEffect(() => {
        if (currenciesData !== undefined) {
            localStorage.removeItem('currencies');
            localStorage.setItem('currencies', JSON.stringify(currenciesData.data.currencies));
            setCurrencies(currenciesData.data.currencies);
        }
    }, [currenciesData]);

    // const [defaultCurrency, setDefaultCurrency] = useState(configJson.currencies[0])
    const [defaultCurrency, setDefaultCurrency] = useState(()=>{
        const localData = localStorage.getItem('defaultCurrency');
        return (localData) ? JSON.parse(localData) : configJson.currencies[0];
    });

    const changeCurrency = (code) => {
        const currency = currencies.find((currency) => {
            return currency.code === code
        });
        if (currency) {
            setDefaultCurrency(currency);
            localStorage.removeItem('defaultCurrency');
            localStorage.setItem('defaultCurrency', JSON.stringify(currency));
        }
    }

    return (
        <CurrencyContext.Provider value={{currencies, changeCurrency, defaultCurrency}}>
            {props.children}
        </CurrencyContext.Provider>
    );
}

export default CurrencyContextProvider;