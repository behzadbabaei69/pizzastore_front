import React, {createContext, useEffect, useState} from 'react'
import configJson from '../config.json';
import {SWRConfig} from "swr";
import axios from 'axios'
import uuid from "uuid/dist/v1";


export const ConfigContext = createContext();

const ConfigContextProvider = (props) => {

    const initialConfig = configJson;

    const [config, setConfig] = useState(() => {
        const localData = localStorage.getItem('config');
        return (localData) ? JSON.parse(localData) : initialConfig;
    })

    const [userToken, setUserToken] = useState(() => {
        const localData = localStorage.getItem('userToken');
        return (localData) ? JSON.parse(localData) : uuid();
    })

    const updateConfig = (name, value) => {
        setConfig({...config, name: value});
    }

    useEffect(() => {
        if (userToken !== undefined) {
            localStorage.removeItem('userToken');
            localStorage.setItem('userToken', JSON.stringify(userToken));
        }
    }, [userToken]);

    axios.defaults.baseURL = config.base_url + '' + config.api_prefix;
    const fetcher = async (url) => await axios(url).then(r => r.data);

    return (
        <ConfigContext.Provider value={{config, updateConfig, userToken, setUserToken}}>
            <SWRConfig value={{
                fetcher: fetcher,
                revalidateOnFocus: false,
                revalidateOnReconnect: false
            }}>
                {props.children}
            </SWRConfig>
        </ConfigContext.Provider>
    );
}

export default ConfigContextProvider;