import React, {createContext, useEffect, useState} from 'react'


export const OrderContext = createContext();
export const OrderContextDispatch = createContext();

const OrderContextProvider = (props) => {

    const [orders, setOrders] = useState(() => {
        const localData = localStorage.getItem('orders')
        return (localData) ? JSON.parse(localData) : []
    });

    const addOrder = (order) => {
        setOrders([...orders, order]);
    };

    const changeOrderType = (order, activeType) => {
        orders.map((item) => {
            if (item.id === order.id) {
                order.order.pizzaTypes.map((pizza) => {
                    if (pizza.id === activeType.id) {
                        pizza.isDefault = true
                        item.order.defaultPizzaType = Object.assign({}, pizza);
                    } else {
                        pizza.isDefault = false
                    }
                })
                item.totalPrice = item.quantity * item.order.defaultPizzaType.price;
                item.totalPrice = item.totalPrice.toFixed(2);
            }
        });
        setOrders([...orders]);
    };

    const addOneOrderQuantity = (order) => {
        orders.map((item) => {
            if (item.id === order.id) {
                item.quantity = item.quantity + 1;
                item.totalPrice = item.quantity * order.order.defaultPizzaType.price;
                item.totalPrice = item.totalPrice.toFixed(2);
            }
        });
        setOrders([...orders]);
    };


    const minOneOrderQuantity = (order) => {
        if (order.quantity > 1) {
            orders.map((item) => {
                if (item.id === order.id) {
                    item.quantity = item.quantity - 1;
                    item.totalPrice = item.quantity * order.order.defaultPizzaType.price;
                    item.totalPrice = item.totalPrice.toFixed(2);
                }
            });
            setOrders([...orders]);
        }
    };

    const deleteOrder = (item) => {
        setOrders([...orders.filter(order => order.id !== item.id)])
    }


    const updateOrderItemsPrices = (availablePizzas) => {
        if (orders.length) {
            orders.map((order) => {
                availablePizzas.map((pizza) => {
                    if (pizza.id === order.order.id) {
                        //for for current default size and replace it with new price
                        let newDefType = pizza.pizzaTypes.find((p) => p.id === order.order.defaultPizzaType.id);
                        if (newDefType) {
                            order.order.defaultPizzaType = Object.assign({}, newDefType)
                            order.order.pizzaTypes = Object.assign([], pizza.pizzaTypes)
                            // order.order.pizzaTypes = pizza.pizzaTypes
                            order.totalPrice = (order.quantity * newDefType.price).toFixed(2);
                        }
                    }
                })
            })
            setOrders([...orders]);
        }
    }


    useEffect(() => {
        localStorage.removeItem('orders');
        localStorage.setItem('orders', JSON.stringify(orders));
    }, [orders])

    return (
        <OrderContext.Provider value={{
            orders
        }}>
            <OrderContextDispatch.Provider
                value={{
                    addOrder,
                    changeOrderType,
                    addOneOrderQuantity,
                    minOneOrderQuantity,
                    deleteOrder,
                    updateOrderItemsPrices,
                    setOrders
                }}>
                {props.children}
            </OrderContextDispatch.Provider>
        </OrderContext.Provider>
    );
}

export default OrderContextProvider;