import React, {createContext, useContext, useEffect, useState} from 'react'
import {CartContextDispatch} from "./CartContext";
import useSWR from "swr";
import {CurrencyContext} from "./CurrencyContext";
import {OrderContextDispatch} from "./OrderContext";

export const PizzaContext = createContext();


const PizzaContextProvider = (props) => {

    const [pizzas, setPizzas] = useState([]);
    const {updateOrderItemsPrices} = useContext(OrderContextDispatch);
    const {defaultCurrency} = useContext(CurrencyContext);
    const {updateCart} = useContext(CartContextDispatch);
    const {data: pizzaData, error} = useSWR('pizza?currency=' + defaultCurrency.code);



    useEffect(() => {
        if (pizzaData !== undefined) {
            localStorage.removeItem('pizzas')
            localStorage.setItem('pizzas', JSON.stringify(pizzaData.data.pizzas));
            setPizzas([...pizzaData.data.pizzas]);
            updateOrderItemsPrices(pizzaData.data.pizzas)
            updateCart()
        }
    }, [pizzaData]);


    return (
        <PizzaContext.Provider value={{pizzas, setPizzas}}>
            {props.children}
        </PizzaContext.Provider>
    );
}

export default PizzaContextProvider;