import React from 'react';
import NavigationBar from "./components/sections/NavigationBar";
import CurrencyContextProvider from "./contexts/CurrencyContext";
import CartContextProvider from "./contexts/CartContext";
import ConfigContextProvider from "./contexts/ConfigContext";
import Footer from "./components/sections/Footer";
import {ToastProvider} from 'react-toast-notifications'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Home from "./components/pages/Home";
import About from "./components/pages/About";
import CheckoutContextProvider from "./contexts/CheckoutContext";
import UserContextProvider from "./contexts/UserContext";
import OrderContextProvider from "./contexts/OrderContext";
import UserOrder from "./components/pages/UserOrder";

function App() {
    return (
        <React.Fragment>
            <Router>
                <ToastProvider >
                    <ConfigContextProvider>
                        <CurrencyContextProvider>
                            <UserContextProvider>
                                <OrderContextProvider>
                                    <CartContextProvider>
                                        <CheckoutContextProvider>
                                            <NavigationBar/>
                                            <Switch>
                                                <Route exact path='/' component={Home}/>
                                                <Route path='/about' component={About}/>
                                                <Route path='/user/orders' component={UserOrder}/>
                                            </Switch>
                                            <Footer/>
                                        </CheckoutContextProvider>
                                    </CartContextProvider>
                                </OrderContextProvider>
                            </UserContextProvider>
                        </CurrencyContextProvider>
                    </ConfigContextProvider>
                </ToastProvider>
            </Router>
        </React.Fragment>
    );
}

export default App;
