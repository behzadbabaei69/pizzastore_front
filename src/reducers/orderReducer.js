import uuid from 'uuid/dist/v1';

export const orderReducer = (state, action) => {
    switch (action.type) {
        case 'ADD_ORDER':
            return [...state, action.payload];
        case 'REMOVE_ORDER':
            return state.filter(order => order.id !== action.id);
        default:
            return state
    }
}
